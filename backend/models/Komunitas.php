<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "komunitas".
 *
 * @property int $id
 * @property string $nama
 * @property string $deskripsi
 * @property string $foto
 * @property string $contact
 * @property string $address
 * @property int $id_admin_komunitas
 * @property string $created
 *
 * @property User $adminKomunitas
 * @property Komunitasmember[] $komunitasmembers
 * @property Project[] $projects
 */
class Komunitas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'komunitas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'deskripsi', 'contact', 'address', 'id_admin_komunitas'], 'required'],
            [['nama', 'deskripsi', 'contact', 'address'], 'string'],
            [['id_admin_komunitas'], 'integer'],
            [['created'], 'safe'],
            [['foto'], 'string', 'max' => 200],
            [['id_admin_komunitas'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_admin_komunitas' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'deskripsi' => 'Deskripsi',
            'foto' => 'Foto',
            'contact' => 'Contact',
            'address' => 'Address',
            'id_admin_komunitas' => 'Id Admin Komunitas',
            'created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminKomunitas()
    {
        return $this->hasOne(User::className(), ['id' => 'id_admin_komunitas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKomunitasmembers()
    {
        return $this->hasMany(Komunitasmember::className(), ['id_komunitas' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['id_komunitas' => 'id']);
    }
}
