<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "donate".
 *
 * @property int $id
 * @property int $id_project
 * @property int $id_user
 * @property string $nama
 * @property string $rekening
 * @property string $amount
 * @property int $status
 * @property string $date
 *
 * @property Project $project
 * @property User $user
 */
class Donate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'donate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_project', 'id_user', 'amount'], 'required'],
            [['id_project', 'id_user', 'status'], 'integer'],
            [['nama', 'rekening'], 'string'],
            [['date'], 'safe'],
            [['amount'], 'string', 'max' => 30],
            [['id_project'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['id_project' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_project' => 'Id Project',
            'id_user' => 'Id User',
            'nama' => 'Nama',
            'rekening' => 'Rekening',
            'amount' => 'Amount',
            'status' => 'Status',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'id_project']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
