<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $email
 * @property string $address
 * @property string $contact
 * @property resource $picture
 * @property string $created_at
 * @property int $role
 * @property string $saldo
 * @property string $updated_at
 * @property string $password_hash
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $status
 *
 * @property Donate[] $donates
 * @property Komunitas[] $komunitas
 * @property Komunitasmember[] $komunitasmembers
 * @property Project[] $projects
 * @property Target[] $targets
 * @property Role $role0
 * @property Volunteer[] $volunteers
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'name', 'email', 'created_at', 'role', 'saldo', 'updated_at', 'password_hash', 'auth_key', 'password_reset_token', 'status'], 'required'],
            [['password', 'name', 'address', 'contact', 'picture', 'created_at', 'updated_at', 'password_hash', 'auth_key', 'password_reset_token', 'status'], 'string'],
            [['role'], 'integer'],
            [['username'], 'string', 'max' => 45],
            [['email'], 'string', 'max' => 255],
            [['saldo'], 'string', 'max' => 11],
            [['role'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'name' => 'Name',
            'email' => 'Email',
            'address' => 'Address',
            'contact' => 'Contact',
            'picture' => 'Picture',
            'created_at' => 'Created At',
            'role' => 'Role',
            'saldo' => 'Saldo',
            'updated_at' => 'Updated At',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDonates()
    {
        return $this->hasMany(Donate::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKomunitas()
    {
        return $this->hasMany(Komunitas::className(), ['id_admin_komunitas' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKomunitasmembers()
    {
        return $this->hasMany(Komunitasmember::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['id_admin_komunitas' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTargets()
    {
        return $this->hasMany(Target::className(), ['user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole0()
    {
        return $this->hasOne(Role::className(), ['id' => 'role']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVolunteers()
    {
        return $this->hasMany(Volunteer::className(), ['id_user' => 'id']);
    }
}
