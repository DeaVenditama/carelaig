<?php

namespace frontend\controllers;

use Yii;
use app\models\Donate;
use app\models\DonateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DonateController implements the CRUD actions for Donate model.
 */
class DonateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Donate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DonateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Donate model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Donate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Donate();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionDonasi($id_project)
    {
      $model = new Donate();
      $projectmodel = new \app\models\Project();
      $project = $projectmodel->findOne($id_project);
      if ($model->load(Yii::$app->request->post()) && $model->save()) {
          $progress_target = $project->progress_target;
          $progress_target += $model->amount;
          $messages='Dear Admin,Komunitas bigo live melakukan donasi '.$model->amount.' buku';
          $nomorhp='081343814229';
          //082291824982
          /*
          Yii::$app->db->createCommand('UPDATE project SET progress_target='.$progress_target.' WHERE id='.$id_project.'')
            ->execute();*/
          $curl = curl_init();

          curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.mainapi.net/smsnotification/1.0.0/messages",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"msisdn\"\r\n\r\n".$nomorhp."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"content\"\r\n\r\n".$messages."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
              "Authorization: Bearer 66fa2015aeca846426549e3baaa6a5f7",
              "Cache-Control: no-cache",
              "Postman-Token: bb826273-2664-bfd3-d3a2-673f55e071d5",
              "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
            ),
          ));

          $response = curl_exec($curl);
          $err = curl_error($curl);

          curl_close($curl);
          $mes='';
          if ($err) {
            echo "cURL Error #:" . $err;
          } else {
            echo $response;
          }
          return $this->redirect(['thanks']);
      }
      $projectmodel = new \app\models\Project();
      $project = $projectmodel->findOne($id_project);
      return $this->render('create',[
        'model'=> $model,
        'id_project'=>$id_project,
        'project'=>$project,
      ]);
    }
    public function actionThanks()
    {
      return $this->render('thanks');
    }
    /**
     * Updates an existing Donate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Donate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Donate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Donate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Donate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
