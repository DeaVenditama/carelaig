<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Komunitasmember */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="komunitasmember-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php

    $id=Yii::$app->user->identity->id;

    echo $form->field($model, 'id_komunitas')
      ->hiddenInput(['value' => $id_komunitas])
      ->label(false);

    echo $form->field($model, 'id_user')
      ->hiddenInput(['value' => $id])
      ->label(false);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
