<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\KomunitasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Komunitas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="komunitas-index">

    <div class="row">
    <?php

      foreach($model as $models)
      {
        $link = Html::a(
          $models->nama,
          Url::to(['/komunitas/view','id'=>$models->id]),
          ['class'=>'tulisannamakomunitas']
        );
        $max_length = 200;
        $s=$models->deskripsi;
        if (strlen($s) > $max_length)
        {
            $offset = ($max_length - 3) - strlen($s);
            $s = substr($s, 0, strrpos($s, ' ', $offset)) . '...';
        }
        $buttondaftar =  Html::a(
                          'Daftar Member',
                          Url::to(['/komunitasmember/create', 'id_komunitas' => $models->id]),
                          [
                              'class'=>'btn btn-primary',
                          ]
                        );
        $buttonreadmore = Html::a(
                              'Read More',
                              Url::to(['/komunitas/view', 'id' => $models->id]),
                              [
                                  'class'=>'btn btn-success',
                              ]
                          );
        echo '<div class="col-lg-4">';
        echo '<div class="box">';
        echo Html::img('@web/uploads/'.$models->foto, ['alt'=>'some', 'class'=>'imageprofilkomunitas']);
        echo '<div style="font-size:30px;"><p class="text-center">'.$link.'</p></div>';
        echo '<div class="paragraf">'.$s.'</div>';
        echo '<div class="paragraf">'.$buttonreadmore.' '.$buttondaftar.'</div>';
        echo '</div>';
        echo '</div>';
      }

    ?>


</div>
<?php
$this->registerCssFile("@web/css/carelaig.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
], 'css-print-theme');
?>
