<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Komunitas */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Komunitas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="komunitas-view">
<h4><?= $model->nama ?></h4>
<div class="row">
  <div class="col-lg-3">
    <div class="box">
      <p class="text-center"><?= Html::img('@web/uploads/'.$model->foto, ['alt'=>'some', 'class'=>'imageprofilkomunitas']);?></p>
      <div class="paragraf">
        <table class="table table-striped">
          <tr>
            <td>admin komunitas : <?= $model->admin->name ?></td>
          </tr>
          <tr>
            <td>kontak : <?= $model->contact ?></td>
          </tr>
          <tr>
            <td>alamat : <?= $model->address ?></td>
          </tr>
          <tr>
            <td>jumlah member : <?= $model->address ?></td>
          </tr>
        </table>
        <p class="text-center"><?= Html::a('Daftar Member', ['/komunitasmember/create', 'id_komunitas' => $model->id], ['class'=>'btn btn-primary']) ?></p>
      </div>
    </div>
  </div>
  <div class="col-lg-9">
    <div class="box">
      <p class="paragraf"><?= $model->deskripsi ?></p>
    </div>
  </div>
</div>

</div>
<?php
$this->registerCssFile("@web/css/carelaig.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
], 'css-print-theme');
?>
