<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Project';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

        <?php

        if(Yii::$app->user->isGuest)
        {

        }else if(Yii::$app->user->identity->role==5){
          echo Html::a('Buat Project Komunitas', ['create'], ['class' => 'btn btn-success']);
        }
        ?>

    <?php
      foreach($model as $models)
      {
        $progress_target = ($models->progress_target/$models->val_target)*100;
        $link = Html::a(
          $models->name,
          Url::to(['/project/view','id'=>$models->id]),
          ['class'=>'tulisannamakomunitas']
        );
        $max_length = 200;
        $s=$models->deskripsi;
        if (strlen($s) > $max_length)
        {
            $offset = ($max_length - 3) - strlen($s);
            $s = substr($s, 0, strrpos($s, ' ', $offset)) . '...';
        }
        echo '<div class="col-lg-4">';
        echo '<div class="box">';
        echo Html::a(Html::img('@web/uploads/'.$models->foto, ['alt'=>'some', 'class'=>'imageprofilkomunitas']),Url::to(['/project/view','id'=>$models->id]));
        echo '<div><h3>'.$link.'</h3></div>';
        echo '<div class="paragraf"><p>'.$s.'</p></div>';
        echo '<div class="targethitam" style="padding-top:5px; "><h4>Target : '.$models->val_target.' '.$models->target->nama.'</h4></div>';
        echo '<div class="pro"><div class="progress"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:'.$progress_target.'%"></div></div></div>';
        echo '<div class="infobawah">'.$models->progress_target.' Terkumpul<br>'.ceil($progress_target).'% Tercukupi<br>&nbsp;</div>';
        echo '</div>';
        echo '</div>';
      }
     ?>
</div>
<?php
  $this->registerCssFile("@web/css/carelaig.css",[
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
  ], 'css-print-theme');
 ?>
