<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\icons\Icon;
/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">
    <div class="row">
      <div class="col-lg-9">
        <div class="box">
          <div class="containerimageprofil">
            <?= Html::img('@web/uploads/'.$model->foto, ['class'=>'imageprofilkomunitas']) ?>
          </div>
          <p class="paragraf"><?= Icon::show('user');  ?></i> <?= $model->admin->name ?>&nbsp;&nbsp;&nbsp;&nbsp;<?= Icon::show('calendar');  ?> <?= date('d/m/Y', strtotime($model->created_date)) ?>&nbsp;&nbsp;&nbsp;&nbsp;<?= Icon::show('map-marker') ?> <?= $model->lokasi ?></p>
          <div class="paragrafartikel">
            <?= $model->deskripsi ?>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="box">
          <?php $progress_target = ($model->progress_target/$model->val_target)*100 ?>
          <div class="pro" style="padding-top:20px;"><div class="progress"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:<?= $progress_target ?>%"></div></div></div>
          <div class="infobawahsamping"><h3><?= $model->progress_target?> dari <?= $model->val_target ?>Terkumpul </h3></div>
          <div class="infobawah"><?= ceil($progress_target)?>% Tercukupi<br>&nbsp;</div>
          <div class="infobawahsamping"><span style="font-size:30px;">10</span><br><small>Donatur</small></div>
          <div class="infobawahsamping"><span style="font-size:30px;">30</span><br><small>Hari Lagi</small></div>
          <div class="pro" style="padding:20px;"><?= Html::a('Donasi', ['donate/donasi', 'id_project' => $model->id], ['class' => 'btn btn-success buttonfull']); ?></div>
        </div>
      </div>
    </div>
    <p>
        <?php
        if(isset(Yii::$app->user->identity->id))
        {
          if($model->id_admin_komunitas==Yii::$app->user->identity->id)
          {
            echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
            echo Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
              ],
            ]);
          }
        }
          ?>
    </p>


</div>
<?php
$this->registerCssFile("@web/css/carelaig.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
], 'css-print-theme');
?>
