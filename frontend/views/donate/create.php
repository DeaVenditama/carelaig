<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Donate */

$this->title = 'Donasi';
$this->params['breadcrumbs'][] = ['label' => 'Donate', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donate-create">
    <?php
      $id;
      if(isset(Yii::$app->user->identity->id))
      {
        $id= Yii::$app->user->identity->id;
      }else {
        $id=null;
      }

    ?>
    <?= $this->render('_form', [
        'model' => $model,
        'id_project' => $id_project,
        'id'=>$id,
        'project'=>$project,
    ]) ?>

</div>
<?php
  $this->registerCssFile("@web/css/carelaig.css",[
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
  ], 'css-print-theme');
 ?>
