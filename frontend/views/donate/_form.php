<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Donate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="donate-form">
  <div class="row">
    <div class="col-lg-8 boxform">
      <h1><p class="text-center">Form Donasi</p></h1>
    <?php $form = ActiveForm::begin(); ?>
    <?php
    echo $form->field($model, 'id_project')
      ->hiddenInput(['value' => $id_project])
      ->label(false);
    $id_user=$id;
    if($id==null)
    {
      $id_user=0;
    }
    echo $form->field($model, 'id_user')
      ->hiddenInput(['value' => $id])
      ->label(false);
    ?>

    <?php
      if($id==null)
      {
        echo $form->field($model, 'nama')->textInput(['placeholder'=>'Input Nama'])->label(false);
      }
      $placeholder='Nomor Rekening';
      if($project->id_target!=1)
      {
        $placeholder='Alamat';
      }
      echo $form->field($model, 'rekening')->textarea(['rows'=>3,'placeholder' => $placeholder])->label(false);
    ?>

    <?= $form->field($model, 'amount')->textInput(['placeholder' => 'Jumlah Donasi'])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Konfirmasi Donasi', ['class' => 'btn btn-success buttonfull']) ?>
    </div>

    <?php ActiveForm::end(); ?>
  </div>
  </div>
</div>
