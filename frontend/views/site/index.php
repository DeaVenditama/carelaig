<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index"

    <div class="atas">

        <div class="row">
            <div class="col-lg-5 header">

                <h2 class="text-left tulisanatas">Post Project Komunitas Anda</h2>
                <h4 class="text-left">dapatkan relawan dan donasi dengan mudah</h4>
                <div id="tombol">
                  <?= Html::a('Project', ['/project'], ['class'=>'btn btn-success']) ?>
                  <?= Html::a('Komunitas', ['/komunitas'], ['class'=>'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
  $this->registerCss("
    .tulisanatas{
      padding-top: 200px;
    }
    .header{
      height: 500px;
      background: url('../web/image/bg-tangan.jpg') no-repeat center center;
      min-height:100%;
      background-size:100px 150px;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      width: 100%;
      -o-background-size: cover;

    }

  ");
?>
