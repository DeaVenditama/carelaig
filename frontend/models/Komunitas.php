<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "komunitas".
 *
 * @property int $id
 * @property string $nama
 * @property string $deskripsi
 * @property resource $foto
 * @property string $contact
 * @property string $address
 * @property int $id_admin_komunitas
 * @property string $created
 *
 * @property User $adminKomunitas
 */
class Komunitas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'komunitas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'deskripsi', 'contact', 'address', 'id_admin_komunitas'], 'required'],
            [['nama', 'deskripsi', 'contact', 'address'], 'string'],
            [['contact'],'string','max'=>'12','min'=>'6'],
            [['foto'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg,jpeg'],
            [['id_admin_komunitas'], 'integer'],
            [['created'], 'safe'],
            [['id_admin_komunitas'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_admin_komunitas' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'deskripsi' => 'Deskripsi',
            'foto' => 'Logo Komunitas',
            'contact' => 'Contact',
            'address' => 'Address',
            'id_admin_komunitas' => 'Id Admin Komunitas',
            'created' => 'Created',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin() {
        return $this->hasOne(User::className(), ['id' => 'id_admin_komunitas']);
    }
}
