<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project".
 *
 * @property int $id
 * @property string $name
 * @property string $deskripsi
 * @property string $target
 * @property string $val_target
 * @property string $date_start
 * @property string $date_end
 * @property string $created_date
 * @property int $id_komunitas
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'deskripsi', 'id_target', 'val_target', 'date_start', 'date_end', 'created_date', 'id_komunitas','id_admin_komunitas'], 'required'],
            [['name', 'deskripsi', 'lokasi', 'lat', 'lng'], 'string'],
            [['foto'], 'file', 'skipOnEmpty' => false, 'extensions'=>'png, jpg, jpeg'],
            [['date_start', 'date_end', 'created_date'], 'safe'],
            [['id_komunitas','id_admin_komunitas','id_target'], 'integer'],
            [['val_target','progress_target'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'deskripsi' => 'Deskripsi',
            'id_target' => 'Target',
            'val_target' => 'Val Target',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'created_date' => 'Created Date',
            'id_komunitas' => 'Id Komunitas',
            'lokasi' => 'Lokasi',
            'lat' => 'Latitude',
            'lng' => 'Longitude',
            'foto' => 'foto',
            'id_admin_komunitas' => 'Id Admin Komunitas',
            'progress_target' => 'Progress Target',
        ];
    }
    public function getAdmin(){
      return $this->hasOne(User::className(),['id'=>'id_admin_komunitas']);
    }
    public function getKomunitas(){
      return $this->hasOne(Komunitas::className(),['id'=>'id_komunitas']);
    }
    public function getTarget(){
      return $this->hasOne(Target::className(),['id'=>'id_target']);
    }

}
