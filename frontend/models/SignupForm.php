<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;
use frontend\models\Role;
/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $role;
    public $name;
    public $address;
    public $contact;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['role', 'required'],
            ['role', 'integer'],

            ['name', 'required'],
            ['name', 'string', 'max'=>255],

            ['address', 'required'],
            ['address', 'string', 'max'=>255],

            ['contact', 'required'],
            ['contact', 'string', 'max'=>255],


        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->role = $this->role;
        $user->username = $this->username;
        $user->email = $this->email;
        $user->name = $this->name;
        $user->address = $this->address;
        $user->contact = $this->contact;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        return $user->save() ? $user : null;
    }
}
